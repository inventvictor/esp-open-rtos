# Component makefile for extras/pwm_ultrasonic

# expected anyone using this driver includes it as 'pwm_ultrasonic/pwm_ultrasonic.h'
INC_DIRS += $(pwm_ultrasonic_ROOT)..

# args for passing into compile rule generation
pwm_ultrasonic_SRC_DIR = $(pwm_ultrasonic_ROOT)

$(eval $(call component_compile_rules,pwm_ultrasonic))
