//Import headers

//Define constants

//Your beloved function
/* This function shoud support the following sensors:
HCSRO4
*/

#include "pwm_ultrasonic.h"

#define MAX_DISTANCE_CM 500 // 5m max

void delay_ms(uint32_t ms)
{
    for (uint32_t i = 0; i < ms; i ++)
        sdk_os_delay_us(1000);
}

float pwm_ultrasonic(int triggerPin, int echoPin, int scale)
{
    uart_set_baud(0, 115200);

    ultrasonic_sensor_t sensor = {
        .trigger_pin = triggerPin,
        .echo_pin = echoPin
    };

    ultrasoinc_init(&sensor);
    int32_t distance = ultrasoinc_measure_cm(&sensor, MAX_DISTANCE_CM);
    if (distance < 0)
    {
        switch (distance)
        {
    	case ULTRASONIC_ERROR_PING:
	    break;
	case ULTRASONIC_ERROR_PING_TIMEOUT:
	    break;
	case ULTRASONIC_ERROR_ECHO_TIMEOUT:
	    break;
        }
    }
    else
    {
        switch (scale)
        {
        case CM:
            return distance;
        case M:
            return distance / 100.0;
        case IN:
            return distance*0.39;
        case FT:
            return distance/30.48;
        }
    }
}
