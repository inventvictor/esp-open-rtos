#ifndef __PWM_ULTRASONIC__
#define __PWM_ULTRASONIC__

#include <espressif/esp_common.h>
#include <esp/uart.h>
#include <esp/gpio.h>
#include <ultrasonic/ultrasonic.h>

#define TRIGGER_PIN 5
#define ECHO_PIN    4

#define M 1
#define CM 2
#define FT 3
#define IN 6

#define MAX_DISTANCE_CM 500 // 5m max

float pwm_ultrasonic(int triggerPin, int echoPin, int scale);
#endif
