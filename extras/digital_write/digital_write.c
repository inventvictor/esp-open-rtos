#include "digital_write.h"

void digital_write(int pin, const bool val){
   gpio_enable(pin, GPIO_OUTPUT);
   gpio_write(pin, val);
}
