# Component makefile for extras/digital_write

# expected anyone using this driver includes it as 'digital_write/digital_write.h'
INC_DIRS += $(digital_write_ROOT)..

# args for passing into compile rule generation
digital_write_SRC_DIR = $(digital_write_ROOT)

$(eval $(call component_compile_rules,digital_write))
