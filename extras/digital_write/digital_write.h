#ifndef __DIGITAL_WRITE__
#define __DIGITAL_WRITE__

#include <esp/gpio.h>
void digital_write(int pin, const bool val);

#endif 
