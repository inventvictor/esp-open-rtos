# Component makefile for extras/onewire_temp

# expected anyone using this driver includes it as 'onewire_temp/onewire_temp.h'
INC_DIRS += $(onewire_temp_ROOT)..

# args for passing into compile rule generation
onewire_temp_SRC_DIR = $(onewire_temp_ROOT)

$(eval $(call component_compile_rules,onewire_temp))
