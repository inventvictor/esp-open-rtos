//Your beloved function
/* This function shoud support the following sensors:
DS18B20
*/

#include "onewire_temp.h"

float onewire_temp (int pinNumber, int scale) {
    ds18b20_addr_t addrs[MAX_SENSORS];
    float temps[MAX_SENSORS];
    int sensor_count;
    int pin_No = pinNumber;
    //uint32_t addr0;
    //uint32_t addr1;
    float temp_c;
    float temp_f;

    gpio_set_pullup(pin_No, true, true);

    sensor_count = ds18b20_scan_devices(pin_No, addrs, MAX_SENSORS);

    if (sensor_count < 1) {
        return -1.0;
    } else {
        if (sensor_count > MAX_SENSORS) sensor_count = MAX_SENSORS;
        for (int i = 0; i < RESCAN_INTERVAL; i++) {
            ds18b20_measure_and_read_multi(pin_No, addrs, sensor_count, temps);
            for (int j = 0; j < sensor_count; j++) {
                //addr0 = addrs[j] >> 32;
                //addr1 = addrs[j];
                temp_c = temps[j];
                temp_f = (temp_c * 1.8) + 32;
		break;
            }
        }
    }

    switch(scale)
    {
	    case(CELSIUS):
		return temp_c;
	    case(FARENHEIT):
		return temp_f;
	    default:
		return 0;
    }
}
