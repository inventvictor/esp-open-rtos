#ifndef __ONEWIRE_TEMP__
#define __ONEWIRE_TEMP__

#include "FreeRTOS.h"
#include "task.h"
#include "esp/uart.h"

#include "ds18b20/ds18b20.h"

#define MAX_SENSORS 1
#define RESCAN_INTERVAL 1
#define LOOP_DELAY_MS 250
#define CELSIUS 2
#define FARENHEIT 1

float onewire_temp(int pinNumber, int scale);

#endif
