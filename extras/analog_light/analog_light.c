//Import headers

//Define constants

//Your beloved function
/* This function shoud support the following sensors:
GA1A12S202
ALS-PT19
*/
#include <stdio.h>
#include <espressif/esp_common.h>
#include <stdlib.h>
#include <math.h>
#include "analog_light.h"

#define MIN_VALUE 800
#define MAX_VALUE 25

float analog_light()
{
  int analog_val = sdk_system_adc_read();
  float vout = (analog_val / 1024) * 3.3;
  return 100 * vout / 3.3;
}
