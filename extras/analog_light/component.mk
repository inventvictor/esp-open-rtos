# Component makefile for extras/analog_light

# expected anyone using this driver includes it as 'analog_light/analog_light.h'
INC_DIRS += $(analog_light_ROOT)..

# args for passing into compile rule generation
analog_light_SRC_DIR = $(analog_light_ROOT)

$(eval $(call component_compile_rules,analog_light))
