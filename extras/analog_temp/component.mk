# Component makefile for extras/analog_temp

# expected anyone using this driver includes it as 'analog_temp/analog_temp.h'
INC_DIRS += $(analog_temp_ROOT)..

# args for passing into compile rule generation
analog_temp_SRC_DIR = $(analog_temp_ROOT)

$(eval $(call component_compile_rules,analog_temp))
