#include "analog_temp.h"

#include <stdio.h>
#include <espressif/esp_common.h>
#include <stdlib.h>

float analog_Temp(int sensorName, int scale)
{
  int analog_val = sdk_system_adc_read();
  analog_val = (analog_val / 1024.0) * 5.0;
  
  if (sensorName == TMP37_SENSOR){
      if (scale == CELSIUS){
          return analog_val / 0.02;
      } else {
         return (analog_val/0.02) * 1.8 + 32;
      }
  } else {
      if (scale == CELSIUS){
         return analog_val / 0.01;
      } else {
         return (analog_val/0.01) * 1.8 + 32;
      }
  }
}
