#ifndef __ANALOG_TEMP__
#define __ANALOG_TEMP__

#include <stdio.h>
#include <stdlib.h>


#define LM35_SENSOR 1
#define TMP36_SENSOR 2
#define TMP37_SENSOR 3
#define TMP35_SENSOR 4

#define CELSIUS 2
#define FARENHEIT 1

float analog_Temp(int sensorName,int scale);



#endif
