//Your beloved function
/* This function shoud support the following sensors:
DHT22
DHT21
DHT11
*/

#include <stdio.h>
#include <stdlib.h>
#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "digital_temp.h"
#include "dht/dht.h"
#include "esp8266.h"

float digital_temp(uint8_t pinNumber, dht_sensor_type_t sensor_type, int scale)
{
  uint8_t dht_gpio = pinNumber;
  int16_t temperature = 0;
  int16_t humidity = 0;

  gpio_set_pullup(dht_gpio, false, false);

  dht_read_data(sensor_type, dht_gpio, &humidity, &temperature);

  switch (scale)
  {
	  case (CELSIUS):
	      return temperature/10;
	  case (FARENHEIT):
	      return (temperature/10 * 1.8) + 32;
	  case (PERCENT):
	      return humidity;
	  default:
	      return 0;
  }
}
