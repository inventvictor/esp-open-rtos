# Component makefile for extras/digital_temp

# expected anyone using this driver includes it as 'digital_temp/digital_temp.h'
INC_DIRS += $(digital_temp_ROOT)..

# args for passing into compile rule generation
digital_temp_SRC_DIR = $(digital_temp_ROOT)

$(eval $(call component_compile_rules,digital_temp))
