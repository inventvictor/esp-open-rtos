
#ifndef __DIGITAL_TEMP__
#define __DIGITAL_TEMP__

#include <dht/dht.h>

#define CELSIUS 2
#define FARENHEIT 1
#define PERCENT 3

float digital_temp (uint8_t pinNumber, dht_sensor_type_t sensor_type, int scale);

#endif 
