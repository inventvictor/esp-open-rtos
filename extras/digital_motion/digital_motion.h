#ifndef __DIGITAL_MOTION__
#define __DIGITAL_MOTION__

#include "espressif/esp_common.h"
#include "esp/uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "esp8266.h"
//Define constants

//Your beloved function
/* This function shoud support the following sensors:
PIR
*/

static QueueHandle_t tsqueue;

void digital_motion(int pin);

#endif
