# Component makefile for extras/digital_motion

# expected anyone using this driver includes it as 'digital_motion/digital_motion.h'
INC_DIRS += $(digital_motion_ROOT)..

# args for passing into compile rule generation
digital_motion_SRC_DIR = $(digital_motion_ROOT)

$(eval $(call component_compile_rules,digital_motion))
